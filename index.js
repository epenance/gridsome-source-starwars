const axios = require('axios');
const camelCase = require('camelcase');
const _cliProgress = require('cli-progress');
const { isPlainObject } = require('lodash');

const TYPES = {
    PEOPLE: {
        typeName: 'Person',
        endpoint: 'people/'
    },
    FILMS: {
        typeName: 'Film',
        endpoint: 'films/'
    },
    STARSHIPS: {
        typeName: 'Starship',
        endpoint: 'starships/'
    },
    VEHICLES: {
        typeName: 'Vehicle',
        endpoint: 'vehicles/'
    },
    SPECIES: {
        typeName: 'Species',
        endpoint: 'species/'
    },
    PLANETS: {
        typeName: 'Planet',
        endpoint: 'planets/'
    }
};

class StarwarsSource {
    static defaultOptions() {
        return {
            baseUrl: 'https://swapi.co/api/',
            typeName: 'Starwars'
        }
    }

    constructor(api, options) {
        this.options = {...StarwarsSource.defaultOptions(), ...options};

        if (!this.options.typeName) {
            throw new Error(`Missing typeName option.`)
        }

        this.http = axios.create({
            baseUrl: this.options.baseUrl
        });

        this.routes = {
            people: '/:name/',
            planet: '/:name/'
        };

        api.loadSource( async store => {
           this.store = store;

           console.log('Loading data from Starwars API');

           const types = Object.keys(TYPES).map((key) => {
               return TYPES[key];
           });


           for (const type of types) {
               await this.getType(type);
           }

        });
    }

    async getType(type) {
        console.log('Fetching ' + type.typeName);
        const bar = new _cliProgress.Bar({
            format: '[{bar}] {percentage}% | ETA: {eta}s'
        }, _cliProgress.Presets.shades_classic);
        bar.start(100, 0);


        const typeName = this.createTypeName(type.typeName);
        const contentType = this.store.addContentType(typeName);

        let next = '';
        let result;

        const {data: res} = await this.http.get(this.options.baseUrl + type.endpoint);

        const { count } = res;
        let processed = 1;

        result = res;

        next = res.next || 'none';

        while(next) {
            for(const unit of result.results) {
                const fields = this.normalizeFields(unit);

                const id = this.getId(unit.url);

                contentType.addNode({
                    id,
                    ...fields
                });

                processed++;

                bar.update(Math.floor(processed / count * 100));
            }

            if(next !== 'none') {
                const {data: res} = await this.http.get(next);
                next = res.next;
                result = res;
            } else {
                next = null;
            }
        }

        bar.update(100);
        bar.stop();
    }

    getId(str) {
        return str.match(/(\d+)/)[0];
    }

    normalizeFields (fields) {
        const res = {}

        for (const key in fields) {
            res[camelCase(key)] =  this.normalizeFieldValue(fields[key], camelCase(key))
        }

        return res
    }

    normalizeFieldValue (value, key) {
        if (value === null) return null;
        if (value === undefined) return null;

        if (Array.isArray(value)) {
            return value.map(v => this.normalizeFieldValue(v, key))
        }

        if(key === 'films') {
            const typeName = this.createTypeName(TYPES.FILMS.typeName);
            const id = this.getId(value);

            return this.store.createReference(typeName, id);
        }

        if(key === 'characters') {
            const typeName = this.createTypeName(TYPES.PEOPLE.typeName);
            const id = this.getId(value);

            return this.store.createReference(typeName, id);
        }

        if(key === 'vehicles') {
            const typeName = this.createTypeName(TYPES.VEHICLES.typeName);
            const id = this.getId(value);

            return this.store.createReference(typeName, id);
        }

        if(key === 'starships') {
            const typeName = this.createTypeName(TYPES.STARSHIPS.typeName);
            const id = this.getId(value);

            return this.store.createReference(typeName, id);
        }

        if(key === 'planets' || key === 'homeworld') {
            const typeName = this.createTypeName(TYPES.STARSHIPS.typeName);
            const id = this.getId(value);

            return this.store.createReference(typeName, id);
        }

        if(key === 'species') {
            const typeName = this.createTypeName(TYPES.SPECIES.typeName);
            const id = this.getId(value);

            return this.store.createReference(typeName, id);
        }

        if(isPlainObject(value)) {
            return this.normalizeFields(value);
        }

        return value;
    }

    createTypeName (name = '') {
        return camelCase(`${this.options.typeName}${name}`, { pascalCase: true })
    }
}

module.exports = StarwarsSource;
