# gridsome-source-starwars
> Fetches and exposes Starwars data for creating example applications

## Install
Install by using your favorite package manager to install the package

```bash
yarn add gridsome-source-starwars
```

```bash
npm install gridsome-source-starwars
```

## How to use
Add the package to the plugin array in your gridsome.config.js file

````javascript
module.exports = {
  siteName: 'My site',
  plugins: [
    {
      use: 'gridsome-starwars-source'
    }
  ],
}
````

### Options
You can change the prefix of the data types by using the `typeName` option, and if you selfhost the Starwars API you can also change the endpoint by setting the `endpoint` option.


````javascript
module.exports = {
  siteName: 'My site',
  plugins: [
    {
      use: 'gridsome-starwars-source',
      options: {
        typeName: 'SW', // Default: Starwars
        endpoint: 'https://localhost:3000' // Default: https://swapi.co/api/
      }       
    }
  ],
}
````
